package comebackisreal.com.pertemuan3latihan1

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import android.telephony.SmsMessage
import android.widget.Toast

class MySMSReceiver : BroadcastReceiver() {

    var arrSMS : MutableList<smsMessage> = ArrayList()
    override fun onReceive(context: Context, intent: Intent) {
        if(intent.action.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)){
            val pdu = (intent.extras.get("pdus") as Array<*>).get(0) as ByteArray
            pdu.let {
                var message = SmsMessage.createFromPdu(it)
                var tmpSMS : smsMessage = smsMessage()
                tmpSMS.phoneNumber = message.displayOriginatingAddress
                tmpSMS.message = message.displayMessageBody
                arrSMS!!.add(tmpSMS)

                Toast.makeText(context, "Phone : ${tmpSMS.phoneNumber} \n " + " Message : ${tmpSMS.message}" + " sekarang ada ${arrSMS!!.count()} item", Toast.LENGTH_LONG).show()
            }

        }
    }
}
