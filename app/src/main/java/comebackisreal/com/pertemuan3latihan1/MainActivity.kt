package comebackisreal.com.pertemuan3latihan1

import android.Manifest
import android.content.Context
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Telephony
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //buat variabel supaya tahu terakhir index keberapa yang udh diambil dari array, jadi g perlu read seluruh arraynya
    var lastIndex : Int = 0

    //buat object receier
    private val smsReceiver = MySMSReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //buat permisi
        //PENTING KALAU TIDAK ADA BARIS INI, PESAN MASUK TIDAK AKAN TERBACA KARENA ANDROID ANGGAP APP KITA G ADA PERMISSION
        //WALAUPUN DIMANIFEST UDAH DIBUAT PERMISSION
        requestPermissions(arrayOf(Manifest.permission.RECEIVE_SMS), 1)

        //walaupun udh buat objek receiver dan udh diset untuk "detect" sms masuk, tetap mau di rigester
        //object smsReceiver ibarat tentara, dan register dibawah ini untuk masukkan tentaranya kedalam mobil tank
        //percuma tentaranya bisa kendarai mobil tank(bisa detect sms) kalo g dikasih mobil tank
        //kira-kira gitu lahh
        registerReceiver(smsReceiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))

        //ini u tau lah ya
        btnRefresh.setOnClickListener {refreshPesan(this@MainActivity)}

    }

    private fun refreshPesan(context: Context){
        //cek kalau jumlah pesan yang diterima dan ditampung dalam array lebih besar dari index yang terakhir diread
        //artinya setelah terakhir kali direfresh ada nambah lagi tapi belum dimunculi
        if(smsReceiver.arrSMS?.count() >= lastIndex){
            //isi nilai i dari indexTerakhir sampai panjang LISTnya
            for(i in lastIndex until smsReceiver.arrSMS!!.count()){
                txtvPesan.text = txtvPesan.text.toString() + "\n " + smsReceiver.arrSMS!![i].phoneNumber + " : " + smsReceiver.arrSMS!![i].message
            }
            //set last index supaya update
            lastIndex = smsReceiver.arrSMS!!.count()
        }
    }

}
